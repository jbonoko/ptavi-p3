#!/usr/bin/python3
# -*- coding = utf-8 -*-

import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import smallsmilhandler
import json
from urllib.request import urlretrieve


class KaraokeLocal():

    def __init__(self, file):
        self.data = []
        self.dicc = {}
        parser = make_parser()
        smilhandler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(smilhandler)
        parser.parse(open(file))
        self.data = smilhandler.get_tags()

    def __str__(self):
        for list in self.data:
            element = []
            for tag in list:
                if tag != "element" and list[tag] != "":
                    element += "\t" + tag + '="' + list[tag] + '"'
            print(list["element"], "".join(element))

    def to_json(self, jsonfile, filename):
        "Archivo json"
        jsonfile = json.dumps(self.data)
        filename = file.split(".")[0] + ".json"
        with open(filename, 'w') as jsonextension:
            json.dump(jsonfile, jsonextension)

    def do_local(self):
        for list in self.data:
            for attribute in list[1]:
                if list[1][attribute][:7] == "http://":
                    urlretrieve(list[attribute])
                    print(list[1][attribute])
                    url = list[1][attribute].split('/')
                    list[1][attribute] = url[-1]
                    print(list[attribute])


if __name__ == "__main__":
    try:
        file = sys.argv[1]
        karaoke = KaraokeLocal(file)
        print(karaoke)
        karaoke.to_json(file, "local.json")
        karaoke.do_local()
        print(karaoke)
    except IndexError:
        sys.exit("Usage:python3 karaoke.py file.smil")
