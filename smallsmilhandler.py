#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.list = []
        self.tags = ['root_layout', 'region', 'img', 'audio', 'textstream']
        self.dicc = {}
        self.dicc['root_layout'] = ['width', 'height', 'background_color']
        self.dicc['region'] = ['id', 'top', 'bottom', 'left', 'right']
        self.dicc['img'] = ['src', 'region', 'begin', 'dur']
        self.dicc['audio'] = ['src', 'begin', 'dur']
        self.dicc['textstream'] = ['src', 'region']

    def startElement(self, name, attrs):
        if name in self.tags:
            dictionary = {}
            dictionary['element'] = name
            for attribute in self.dicc[name]:
                dictionary[attribute] = attrs.get(attribute, "")
            self.list.append(dictionary)

    def get_tags(self):
        return self.list


if __name__ == "__main__":

    parser = make_parser()
    smilHandler = SmallSMILHandler()
    parser.setContentHandler(smilHandler)
    parser.parse(open('karaoke.smil'))
    print(smilHandler.get_tags())
